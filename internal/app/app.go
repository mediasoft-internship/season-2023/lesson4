package app

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"lesson4/internal/bootstrap"
	"lesson4/internal/config"
	"lesson4/internal/repositories/employeerepository/employeegorm"
	"lesson4/internal/services/employeeservice"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func Run(cfg config.Config) error {

	db, err := bootstrap.InitGormDB(cfg)
	if err != nil {
		return err
	}

	emplService := employeeservice.New(employeegorm.New(db))

	server := &http.Server{
		Addr:    fmt.Sprintf("0.0.0.0:%d", cfg.Port),
		Handler: emplService.GetHandler(),
	}

	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		if err := server.ListenAndServe(); err != nil {
			log.Fatal(err)
		}
	}()

	gracefullyShutdown(ctx, cancel, server)
	return nil
}

func gracefullyShutdown(ctx context.Context, cancel context.CancelFunc, server *http.Server) {
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	defer signal.Stop(ch)
	<-ch
	if err := server.Shutdown(ctx); err != nil {
		log.Warning(err)
	}
	cancel()
}
